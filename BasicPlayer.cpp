#include "stdafx.h"
#include "BasicPlayer.h"
#include <cstdlib>


BasicPlayer::BasicPlayer(void)
{
	Hand h = Hand();
}

void BasicPlayer::addCard(Card c)
{
	h.add(c);
}
void BasicPlayer::addHand(Hand hand)
{
	h = h+hand;
}
int BasicPlayer::cardsLeft()
{
	return h.size();
}
Bid BasicPlayer::playHand(Bid b)
{
	return chooseBid(b, cheat(b));
}
bool BasicPlayer::callCheat (Bid b)
{
	if(h.countRank(b.getRank()) >= 4)
	{
		return true;
	}
	else
	{
		return false;
	}
}
bool BasicPlayer::cheat(Bid b)
{
	h.sortAscending();
	for (Card c : h.getHand())
	{
		//If cards are equal value, do not cheat
		if(c.getRank() == b.getRank())
		{
			return false;
		}
		//If card value is one higher than bid, do not cheat

		if (c.getRank() == b.getNext())
		{
			return false;
		}
	}
	return true;
}
Bid BasicPlayer::chooseBid (Bid b, bool cheat)
{
	Hand bidHand = Hand();
	if(cheat){
		int random = rand() % h.size();
		Card c;
		c = h.getHand().at(random); 
		bidHand.add(c);
		h.remove(c);
		b = Bid(bidHand, c.getRank());
		return b;
	}
	else
	{
		h.sortAscending();
		for (Card c : h.getHand())
		{
			if(c.getRank() == b.getRank())
			{
				bidHand.add(c);
			}
		}
		if (bidHand.size() == 0)
		{
			for (Card c : h.getHand())
			{
				if (c.getRank() == b.getNext())
				{
					bidHand.add(c);
				}
			}
		}
		for(Card c : bidHand.getHand())
		{
			h.remove(c);
		}
		b = Bid(bidHand, bidHand.getHand().at(0).getRank());
		return b;
	}
}