#pragma once
#include "Player.h"
class BasicPlayer : public Player
{
private:
	Hand h;

public:
	BasicPlayer(void);
	void addCard(Card c);
	void addHand(Hand hand);
	int cardsLeft();
	Bid playHand(Bid b);
	bool callCheat (Bid b);
	bool cheat(Bid b);
	Bid chooseBid (Bid b, bool cheat);

};

