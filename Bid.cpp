#include "stdafx.h"
#include "Bid.h"
#include <iostream>

using namespace std;

Bid::Bid()
{
	h = Hand();
	r = Card::Rank::TWO;
}
Bid::Bid(Hand hand, Card::Rank bidRank)
{
	h = hand;
	r = bidRank;
}

void Bid::setHand(Hand hand)
{
	h = hand;
}
void Bid::setRank(Card::Rank rank)
{
	r = rank;
}
bool Bid::isValid()
{
	for (Card c : h.getHand())
	{
		if(c.getRank() != r)
		{
			return false;
		}
	}
	return true;
}

ostream& operator << (ostream& o, Bid& b)
{

	o<<b.getHand().size()<< " x " << b.getRank();
	return o;
}
istream& operator >> (istream& i, Bid& b)
{
	return i;
}