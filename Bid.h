#pragma once
#include "Hand.h"

class Bid
{
private:
	Hand h;
	Card::Rank r;

public:
	Bid();
	Bid(Hand hand, Card::Rank bidRank);

	void setHand(Hand hand);
	void setRank(Card::Rank rank);
	bool isValid ();

/////////////////////// Hidden Header /////////////////////
	inline Hand getHand() const;
	inline Card::Rank getRank() const;
	inline Card::Rank getNext() const;
	inline int getCount();
//////////////////////////////////////////////////////////
};

std::ostream& operator << (std::ostream& o, Bid& b);
std::istream& operator >> (std::istream& i, Bid& b);
#include "Bid.hh"