#pragma once

#include "stdafx.h"
#include <iostream>
#include "Card.h"
#include <string>
#include <algorithm>

using namespace std;

//constructor creating a Card object with defaults set to Ace of Spades
Card::Card()
{
	this->rank = Card::Rank::ACE;
	this->suit = Card::Suit::SPADES;
}
//constructor creating a Card object with defaults set to Ace of Spades
Card::Card(Rank rank = Rank::ACE, Suit suit = Suit::SPADES)
{
	this->rank = rank;
	this->suit = suit;
}

void Card::setSuit(Suit inputSuit)
{
	suit = inputSuit;
}

void Card::setRank(Rank inputRank)
{
	rank = inputRank;
}

Card::operator int()
{
	int i = static_cast<int>(rank);
	if (i <= 8)
	{
		i= i+2;
	}
	else if(i <= 11)
	{
		i = 10;
	}
	else {
		i = 11;
	}
	return i;
}

//Operator overloaders
ostream& operator << (ostream& o, const Card c)
{	
	string rankString = "";
	string suitString = "";
	//setting rank
	if(c.getRank() == Card::Rank::TWO)
	{
		rankString = "two";
	}
	else if(c.getRank() == Card::Rank::THREE)
	{
		rankString = "three";
	}
	else if(c.getRank() == Card::Rank::FOUR)
	{
		rankString = "four";
	}
	else if(c.getRank() == Card::Rank::FIVE)
	{
		rankString = "five";
	}
	else if(c.getRank() == Card::Rank::SIX)
	{
		rankString = "six";
	}
	else if(c.getRank() == Card::Rank::SEVEN)
	{
		rankString = "seven";
	}
	else if(c.getRank() == Card::Rank::EIGHT)
	{
		rankString = "eight";
	}
	else if(c.getRank() == Card::Rank::NINE)
	{
		rankString = "nine";
	}
	else if(c.getRank() == Card::Rank::TEN)
	{
		rankString = "ten";
	}
	else if(c.getRank() == Card::Rank::JACK)
	{
		rankString = "jack";
	}
	else if(c.getRank() == Card::Rank::QUEEN)
	{
		rankString = "queen";
	}
	else if(c.getRank() == Card::Rank::KING)
	{
		rankString = "king";
	}
	else if(c.getRank() == Card::Rank::ACE)
	{
		rankString = "ace";
	}
	//setting suit
	if(c.getSuit() == Card::Suit::CLUBS)
	{
		suitString = "clubs";
	}
	else if(c.getSuit() == Card::Suit::DIAMONDS)
	{
		suitString = "diamonds";
	}
	else if(c.getSuit() == Card::Suit::HEARTS)
	{
		suitString = "hearts";
	}
	else if(c.getSuit() == Card::Suit::SPADES)
	{
		suitString = "spades";
	}

	return o << rankString << " of " << suitString;
}

istream& operator >> (istream& i, Card& c)
{
	string rankString, of, suitString;

	if(i >> rankString >> of >> suitString)
	{
		transform(rankString.begin(), rankString.end(), rankString.begin(), ::tolower);	
		transform(suitString.begin(), suitString.end(), suitString.begin(), ::tolower);	

		//setting the rank
		if (rankString == "two")
		{
			c.setRank(Card::Rank::TWO);
		}
		else if (rankString == "three")
		{
			c.setRank(Card::Rank::THREE);
		}
		else if (rankString == "four")
		{
			c.setRank(Card::Rank::FOUR);
		}
		else if (rankString == "five")
		{
			c.setRank(Card::Rank::FIVE);
		}
		else if (rankString == "six")
		{
			c.setRank(Card::Rank::SIX);
		}
		else if (rankString == "seven")
		{
			c.setRank(Card::Rank::SEVEN);
		}
		else if (rankString == "eight")
		{
			c.setRank(Card::Rank::EIGHT);
		}
		else if (rankString == "nine")
		{
			c.setRank(Card::Rank::NINE);
		}
		else if (rankString == "ten")
		{
			c.setRank(Card::Rank::TEN);
		}
		else if (rankString == "jack")
		{
			c.setRank(Card::Rank::JACK);
		}
		else if (rankString == "queen")
		{
			c.setRank(Card::Rank::QUEEN);
		}
		else if (rankString == "king")
		{
			c.setRank(Card::Rank::KING);
		}
		else if (rankString == "ace")
		{
			c.setRank(Card::Rank::ACE);
		}
		else
		{
			cout << rankString << " is an invalid card, please use the format <rank> of <suit>" <<endl;
			i.clear(ios_base::failbit);
		}

		//setting the suit
		if(suitString == "clubs")
		{
			c.setSuit(Card::Suit::CLUBS);
		}
		else if(suitString == "diamonds")
		{
			c.setSuit(Card::Suit::DIAMONDS);
		}
		else if(suitString == "hearts")
		{
			c.setSuit(Card::Suit::HEARTS);
		}
		else if (suitString == "spades")
		{
			c.setSuit(Card::Suit::SPADES);
		}
		else
		{
			cout << suitString << " is an invalid card, please use the format <rank> of <suit>" <<endl;
			i.clear(ios_base::failbit);
		}
	}
	return i;
}

ostream& operator << (ostream& o, const Card::Suit s)
{
	string suitString = "";
	if(s == Card::Suit::CLUBS)
	{
		suitString = "clubs";
	}
	else if(s == Card::Suit::DIAMONDS)
	{
		suitString = "diamonds";
	}
	else if(s == Card::Suit::HEARTS)
	{
		suitString = "hearts";
	}
	else if(s == Card::Suit::SPADES)
	{
		suitString = "spades";
	}
	return o << suitString;
}
std::istream& operator >> (std::istream& i, Card::Suit& s)
{
	string suitString;
	if (i >> suitString)
	{
		transform(suitString.begin(), suitString.end(), suitString.begin(), ::tolower);	

		if(suitString == "clubs")
		{
			s = Card::Suit::CLUBS;
		}
		else if(suitString == "diamonds")
		{
			s = Card::Suit::DIAMONDS;
		}
		else if(suitString == "hearts")
		{
			s = Card::Suit::HEARTS;
		}
		else if (suitString == "spades")
		{
			s = Card::Suit::SPADES;
		}
		else
		{
			cout << suitString << " is an invalid suit, please enter a valid suit" <<endl;
			i.clear(ios_base::failbit);
		}

	}
	return i;
}

std::ostream& operator << (std::ostream& o, const Card::Rank r)
{
	string rankString = "";

	if(r == Card::Rank::TWO)
	{
		rankString = "two";
	}
	else if(r == Card::Rank::THREE)
	{
		rankString = "three";
	}
	else if(r == Card::Rank::FOUR)
	{
		rankString = "four";
	}
	else if(r == Card::Rank::FIVE)
	{
		rankString = "five";
	}
	else if(r == Card::Rank::SIX)
	{
		rankString = "six";
	}
	else if(r == Card::Rank::SEVEN)
	{
		rankString = "seven";
	}
	else if(r == Card::Rank::EIGHT)
	{
		rankString = "eight";
	}
	else if(r == Card::Rank::NINE)
	{
		rankString = "nine";
	}
	else if(r == Card::Rank::TEN)
	{
		rankString = "ten";
	}
	else if(r == Card::Rank::JACK)
	{
		rankString = "jack";
	}
	else if(r == Card::Rank::QUEEN)
	{
		rankString = "queen";
	}
	else if(r == Card::Rank::KING)
	{
		rankString = "king";
	}
	else if(r == Card::Rank::ACE)
	{
		rankString = "ace";
	}

	return o << rankString;
}
std::istream& operator >> (std::istream& i, Card::Rank& r)
{
		string rankString;

	if(i >> rankString)
	{
		transform(rankString.begin(), rankString.end(), rankString.begin(), ::tolower);	

	if (rankString == "two")
	{
		r = Card::Rank::TWO;
	}
	else if (rankString == "three")
	{
		r = Card::Rank::THREE;
	}
	else if (rankString == "four")
	{
		r = Card::Rank::FOUR;
	}
	else if (rankString == "five")
	{
		r = Card::Rank::FIVE;
	}
	else if (rankString == "six")
	{
		r = Card::Rank::SIX;
	}
	else if (rankString == "seven")
	{
		r = Card::Rank::SEVEN;
	}
	else if (rankString == "eight")
	{
		r = Card::Rank::EIGHT;
	}
	else if (rankString == "nine")
	{
		r = Card::Rank::NINE;
	}
	else if (rankString == "ten")
	{
		r = Card::Rank::TEN;
	}
	else if (rankString == "jack")
	{
		r = Card::Rank::JACK;
	}
	else if (rankString == "queen")
	{
		r = Card::Rank::QUEEN;
	}
	else if (rankString == "king")
	{
		r = Card::Rank::KING;
	}
	else if (rankString == "ace")
	{
		r = Card::Rank::ACE;
	}
	else
	{
		cout << rankString << " is an invalid rank, please enter a valid rank" <<endl;
		i.clear(ios_base::failbit);
	}
}
	return i;
}