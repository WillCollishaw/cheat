#pragma once

class Card
{
public:
	enum Suit {CLUBS, DIAMONDS, HEARTS, SPADES};
	enum Rank {TWO , THREE, FOUR, FIVE , SIX, SEVEN , 
		EIGHT, NINE , TEN , JACK , QUEEN , KING , ACE };

private:
	Rank rank;
	Suit suit;

public:
	//constructor
	Card();
	Card(Rank rank, Suit suit);

	void setRank(Rank inputRank);
	void setSuit (Suit inputSuit);
	operator int();

/////////////////////// Hidden Header /////////////////////
	inline Suit getSuit() const;
	inline Rank getRank() const;
	inline Rank getNext() const;
};

inline bool operator == (const Card& o1,  const Card& o2);
inline bool operator != (const Card& o1,  const Card& o2);
inline bool operator < (const Card& o1,  const Card& o2);
inline bool operator > (const Card& o1,  const Card& o2);
inline bool operator <= (const Card& o1,  const Card& o2);
inline bool operator >= (const Card& o1,  const Card& o2);

//////////////////////////////////////////////////////////
std::ostream& operator << (std::ostream& o, const Card c);
std::istream& operator >> (std::istream& i, Card& c);

std::ostream& operator << (std::ostream& o, const Card::Suit s);
std::istream& operator >> (std::istream& i, Card::Suit& s);

std::ostream& operator << (std::ostream& o, const Card::Rank r);
std::istream& operator >> (std::istream& i, Card::Rank& r);

#include "Card.hh"