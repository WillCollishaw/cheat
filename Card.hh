#pragma once 

inline Card::Rank Card::getRank() const 
{
	return rank;
}

inline Card::Suit Card::getSuit() const 
{
	return suit;
}

inline Card::Rank Card::getNext() const
{;
return (Card::Rank)(rank+1);
}

inline bool operator == (const Card& o1,  const Card& o2)
{
	if (o1.getRank() == o2.getRank() && o1.getSuit() == o2.getSuit())
	{
		return true;
	}
	else
	{
		return false;
	}
}

inline bool operator != (const Card& o1,  const Card& o2)
{
	if (o1.getRank() != o2.getRank() && o1.getSuit() != o2.getSuit())
	{
		return true;
	}
	else 
	{
		return false;
	}
}

inline bool operator < (const Card& o1,  const Card& o2)
{
	int firstRank = o1.getRank();
	int secondRank = o2.getRank();
	int firstSuit = o1.getSuit();
	int secondSuit = o2.getSuit();
	if (firstRank < secondRank)
	{
		return true;
	}
	else if (firstRank > secondRank)
	{
		return false;
	}
	else if (firstSuit < secondSuit)
	{
		return true;
	}
	else
	{
		return false;
	}
}

inline bool operator > (const Card& o1,  const Card& o2)
{
	int firstRank = o1.getRank();
	int secondRank = o2.getRank();
	int firstSuit = o1.getSuit();
	int secondSuit = o2.getSuit();
	if (firstRank > secondRank)
	{
		return true;
	}
	else if (firstRank < secondRank)
	{
		return false;
	}
	else if(firstSuit > secondSuit)
	{
		return true;
	}
	else
	{
		return false;
	}
}

inline bool operator <= (const Card& o1,  const Card& o2)
{
	if(o1 == o2)
	{
		return true;
	}

	if (o1 < o2)
	{
		return true;
	}
	else
	{
		return false;
	}
}

inline bool operator >= (const Card& o1,  const Card& o2)
{

	if(o1 == o2)
	{
		return true;
	}

	if (o1 > o2)
	{
		return true;
	}
	else
	{
		return false;
	}
}