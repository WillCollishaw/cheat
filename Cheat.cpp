#include "stdafx.h"
#include "Player.h"
#include "Deck.h"
#include "BasicPlayer.h"
#include "HumanPlayer.h"
#include "Player.h"
#include <iostream>
#include <vector>
#include <string>

using namespace std;

//vector<BasicPlayer> players;
vector<HumanPlayer> players;
int nosPlayers;
static const int MINPLAYERS = 5;
int currentPlayer;
Hand discards;
Bid currentBid;

//void basicCheat (int n)
//{
//	nosPlayers = n;
//	for (int i = 0; i < nosPlayers; i++)
//	{
//		players.push_back(BasicPlayer());
//		players.at(i) = BasicPlayer();
//	}
//	currentBid = Bid();
//	currentBid.setRank(Card::Rank::TWO);
//	currentPlayer = 0;
//}

void humanCheat (int n)
{
	nosPlayers = n;
	for (int i = 0; i < nosPlayers; i++)
	{
		players.push_back(HumanPlayer());
		players.at(i) = HumanPlayer();
	}
	currentBid = Bid();
	currentBid.setRank(Card::Rank::TWO);
	currentPlayer  = 0;
}

static bool isCheat(Bid b)
{
	for (Card c : b.getHand().getHand())
	{
		if (c.getRank() != b.getRank())
		{
			return true;
		}
	}
	return false;
}


bool playTurn()
{
	cout << "Current bid = " << currentBid << endl;
	currentBid = players.at(currentPlayer).playHand(currentBid);
	cout << "Player bid = " << currentBid << endl;

	discards.add(currentBid.getHand());
	bool cheat = false;
	for (int i = 0; i < players.size() && !cheat; i++)
	{
		if (i != currentPlayer)
		{
			cheat = players.at(i).callCheat(currentBid);
			if (cheat)
			{
				cout << "Player called cheat by Player " << (i + 1) << endl;
				if (isCheat(currentBid)) 
				{
					players.at(currentPlayer).addHand(discards);
					cout << "Player Cheats" << endl;
					cout << "Adding Cards to player " << (currentPlayer + 1) << endl;
				}
				else
				{
					cout << "Player Honest" << endl;
					players.at(currentPlayer).addHand(discards);
					cout << "Adding Cards to player " << (currentPlayer + 1) << endl;
				}
				currentBid = Bid();
				discards = Hand();
			}
		}

	}
	if (!cheat)
	{
		cout << "No Cheat Called" << endl;
		currentPlayer = (currentPlayer +1) % nosPlayers;
	}
	return true;
}
int winner()
{
	for (int i = 0; i <nosPlayers; i++) 
	{
		if(players.at(i).cardsLeft() == 0)
		{
			return i;
		}
	}
	return -1;
}

void initialise()
{
	Deck d = Deck();
	d.shuffle();
	int count = 1;
	for (Card c : d.getDeck())
	{
		players.at(count-1).addCard(d.deal());
		if (count == players.size())
		{
			count = 1;
		} else
		{
			count++;
		}
	}

	discards = Hand();
	currentPlayer = 0;
	currentBid = Bid();
	currentBid.setRank(Card::Rank::TWO);
}

void playGame()
{
	initialise();
	int c = 0;
	bool finished = false;
	while (!finished)
	{
		cout << "Cheat turn for player " << (currentPlayer + 1) << endl;
		playTurn();
		cout << "Current discards =\n" << discards << endl;
		c++;

		cout << " Turn " << c << " Complete. Press any key to continue or enter Q to quit>" << endl;

		string str;
		cin >> str;

		if(str == "q"||str == "Q"||str == "quit")
		{
			finished = true;
		}
	}

	int w = winner();
	if(w >= 0)
	{
		cout << "The Winner is Player " << (w+1) << endl;
		finished = true;
	}

}

int main(){

	//basicCheat(5);
	humanCheat(5);
	playGame();

}
