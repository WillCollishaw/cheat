#include "stdafx.h"
#include "Deck.h"
#include <algorithm>
#include <iostream>

using namespace std;
Deck::Deck()
{
	for (int i = 0; i < 4; i++)
	{
		for (int k = 0; k < 13; k++)
		{
			Card::Rank rank = static_cast<Card::Rank>(k);
			Card::Suit suit = static_cast<Card::Suit>(i);
			deck.push_back(Card(rank, suit));
		}
	}
}

Card Deck::deal()
{
	Card c = deck.at(0);
	deck.erase(deck.begin());
	return c;
}

void Deck::shuffle()
{
	random_shuffle(deck.begin(), deck.end());
}

int Deck::size()
{
	return deck.size();
}

ostream& operator << (ostream& o, Deck d)
{

	for (Card c : d.getDeck())
	{
		o << c << "\n";
	}
	return o;
}

istream& operator >> (istream& i, Deck& d)
{
	return i;
}
