#pragma once
#include <vector>
#include "Card.h"
class Deck
{
private:
	std::vector<Card> deck;
public:
	Deck();

	Card deal();
	void shuffle();
	int size();

/////////////////////// Hidden Header /////////////////////
	inline std::vector<Card> getDeck() const;

//////////////////////////////////////////////////////////
};

std::ostream& operator << (std::ostream& o, Deck d);
std::istream& operator >> (std::istream& i, Deck& d);

#include "Deck.hh"