#include "stdafx.h"
#include "Hand.h"
#include <algorithm>
#include <functional>
#include <iostream>
using namespace std;

Hand::Hand()
{
	vector<Card> hand;
	numberCount[13];
	for (int i = 0; i < 12; i++)
	{
		numberCount[i] = 0;
	}
}

void Hand::add(Card c)
{
	hand.push_back(c);
	numberCount[c.getRank()]++;
}

void Hand::add(Hand h)
{
	for (Card c : h.getHand())
	{
		this->add(c);
		h.remove(c);
	}
}
void Hand::remove(Card c)
{
	for (int i = 0; i < hand.size(); i++)
	{
		if (c.getRank() == hand.at(i).getRank() && c.getSuit()== hand.at(i).getSuit())
		{
			hand.erase(hand.begin()+i);
			numberCount[c.getRank()]--;
		}
	}
}

void Hand::sortAscending()
{
	sort(hand.begin(), hand.end());
}

void Hand::sortDescending()
{
	sort(hand.begin(), hand.end(), greater<int>());
}
int Hand::countRank(Card::Rank r)
{
	int count = 0;
	for (Card c : hand)
	{
		if (c.getRank() == r)
		{
			count++;
		}
	}
	return count;
}

int Hand::size()
{
	return hand.size();
}
bool Hand::isFlush()
{
	for (int i = 1; i < hand.size(); i++)
	{
		if(hand.at(i).getSuit() != hand.at(0).getSuit())
		{
			return false;
		}
	}
	return true;
}
bool Hand::isStraight()
{
	this->sortAscending();
	for (int i = 1; i < hand.size(); i++)
	{
		if (hand.at(i-1).getRank() != hand.at(i).getRank()-1)
		{
			return false;
		}
	}
	return true;
}


ostream& operator << (ostream& o, Hand& h)
{

	for (Card c : h.getHand())
	{
		o << c << "\n";
	}
	return o;
}

istream& operator >> (istream& i, Hand& h)
{
	return i;
}
