#pragma once
#include <vector>
#include "Card.h"
class Hand
{
private:
	std::vector<Card> hand;
	int numberCount[13];
public:
	Hand();

	void add(Card c);
	void add(Hand h);
	void remove(Card c);
	void sortAscending();
	void sortDescending();
	int countRank(Card::Rank r);
	int size();
	bool isFlush();
	bool isStraight();
	

/////////////////////// Hidden Header /////////////////////
	inline std::vector<Card> getHand() const;
};

inline Hand operator + (Hand& h1,  Hand& h2);
//////////////////////////////////////////////////////////

std::ostream& operator << (std::ostream& o, Hand& c);
std::istream& operator >> (std::istream& i, Hand& c);
#include "Hand.hh"