#pragma once

inline std::vector<Card> Hand::getHand() const
{
	return hand;
}

inline Hand operator + (Hand& h1, Hand& h2)
{
	Hand h = Hand();
	for(Card c : h1.getHand())
	{
		h.add(c);
	}

	for (Card c : h2.getHand())
	{
		h.add(c);
	}
	return h;
}