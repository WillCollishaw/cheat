#include "stdafx.h"
#include "HumanPlayer.h"
#include <iostream>
#include <string>
#include <algorithm>
using namespace std;

//Human Player Constructor
HumanPlayer::HumanPlayer(void)
{
	Hand h = Hand();
}

void HumanPlayer::addCard(Card c)
{
	h.add(c);
}
void HumanPlayer::addHand(Hand hand)
{
	h = h+hand;
}
int HumanPlayer::cardsLeft()
{
	return h.size();
}
Bid HumanPlayer::playHand(Bid b)
{
	return chooseBid(b, cheat(b));
}
bool HumanPlayer::callCheat(Bid b)
{
	cout << "\nNext Player, this is your current hand:" <<endl;
	h.sortAscending();

	cout << h << endl;

	cout << "This is the current bid:" << endl;
	cout << b.getHand().size() << " X " << b.getRank() << endl;
	cout << "\nWould you like to call cheat?" << endl;

	bool validAnswer = false;
	while (!validAnswer)
	{
		string answer;
		cin.clear();
		cin >> answer;

		transform(answer.begin(), answer.end(), answer.begin(), ::tolower);	

		if (answer == "y" || answer == "yes")
		{
			validAnswer = true;
			return true;
		}
		else if (answer == "n"|| answer == "no")
		{
			validAnswer = true;
			return false;
		}
		else
		{
			cout << "Please enter a valid answer" << endl;
		}
	}
}
bool HumanPlayer::cheat(Bid b)
{
	bool canPlay = false;
	h.sortAscending();

	cout<< "\nThis is your current hand:" <<endl;
	cout<< h <<endl;
	//When asked to cheat, if the bid hand is empty the player is asked 
	//if they would like to cheat
	if (b.getHand().size() == 0)
	{
		for (Card c : h.getHand())
		{
			//If the bid hand is empty and the player has a two or a three in their hand
			//they can choose to cheat or not. If the player does not have one of these
			//cards theymust cheat 
			if (c.getRank() == Card::Rank::TWO || c.getRank() == Card::Rank::THREE)
			{
				canPlay = true;
			}
		}
		if(canPlay) 
		{
			cout << "Would you like to cheat? enter 'Y' or 'N'" << endl;

			bool validAnswer = false;
			while (!validAnswer)
			{
				string answer;
				cin.clear();
				cin >> answer;

				transform(answer.begin(), answer.end(), answer.begin(), ::tolower);	

				if (answer == "y" || answer == "yes")
				{
					validAnswer = true;
					return true;
				}
				else if (answer == "n"|| answer == "no")
				{
					validAnswer = true;
					return false;
				}
				else
				{
					cout << "Please enter a valid answer" << endl;
				}
			}
		}
		else
		{
			cout << "You must cheat!" << endl;
		}
		return true;
	}
	//For each loop checking if the play can play without cheating if 
	//the bid hand was NOT empty
	for (Card c : h.getHand())
	{
		if (c.getRank() == b.getRank() || c.getRank() == b.getNext())
		{
			cout << "You must cheat!" << endl;
			return true;
		}
	}

	//If player can play without cheating, the player has the option to 
	//cheat or not
	cout << "Would you like to cheat? enter 'Y' or 'N'" << endl;

	bool validAnswer = false;
	while (!validAnswer)
	{
		string answer;
		cin.clear();
		cin >> answer;

		transform(answer.begin(), answer.end(), answer.begin(), ::tolower);	

		if (answer == "y" || answer == "yes")
		{
			validAnswer = true;
			return true;
		}
		else if (answer == "n"|| answer == "no")
		{
			validAnswer = true;
			return false;
		}
		else
		{
			cout << "Please enter a valid answer" << endl;
		}
	}
}
//////////////////////////////////////////////////////////////////////////////
/*
* Variables used in chooseBid() that must not change if recursion is 
* called. So the variables are initiated outside of the method.
*/
Hand bidHand = Hand();
int cheatersCardCount = 1;
bool addBoolean = false;
Card::Rank rankChecker; //may not need
Card::Rank cheatingRank;//may not need
string endTurn;
//////////////////////////////////////////////////////////////////////////////
Bid HumanPlayer::chooseBid(Bid b, bool cheat)
{
	//If the player has chosen to cheat all of their cards will be printed out as 
	//they can cheat with any card they like.  
	if (cheat)
	{
		cout <<"You have chosen to cheat,these are the cards you can cheat with: " << endl;
		h.sortAscending();
		cout << h << endl;
	}
	//If the player has chosen to not cheat, the cards they can play without 
	//cheating will be printed out
	else 
	{

		cout << "You are not cheating, these are the cards you can play: " << endl;
		h.sortAscending();
		//////////////////////////////////////////////////////////////////////////////
		//This if is only true if the player is adding their second, third or fourth//
		//card and is not cheating, this will print out the cards they own of the   //          
		// rank of the initial card they played.                                    //
		//////////////////////////////////////////////////////////////////////////////
		if (addBoolean) 
		{
			for (Card c : h.getHand()) 
			{
				if (c.getRank() == rankChecker) 
				{
					cout << c << endl;
				}
			}
			//If the player is adding their first card and their was no previous bids,
			//the players two's and three's will be printed
		} 
		else if (b.getHand().size() == 0) 
		{
			for (Card c : h.getHand()) 
			{
				if (c.getRank() == Card::Rank::TWO || c.getRank() == Card::Rank::THREE) 
				{
					cout << c <<endl;
				}
			}
		}
		else 
		{
			//If the player is adding their first card, this will print their cards of
			//equal rank of the bid as well as the following rank  
			for (Card c : h.getHand())
			{
				if (c.getRank() == b.getRank()) 
				{
					cout <<c << endl;
				}
				else if (c.getRank() == b.getNext())
				{
					cout <<c << endl;
				}
			}
		}
	}
	bool correctRank = false;
	bool rankBool = false;
	Card::Rank bidRank;

	/*
	*Checking rank input is valid 
	*/
	cout <<"First, the RANK of the card you would like to play" << endl;
	//Start of error checking for the RANK input
	while (!correctRank) 
	{
		cin.clear();
		cin >> bidRank;

		//This will try to create a card using the rank the player has input.
		//If the player has input an invalid rank name the try will fail, and the 
		//while loop will restart, telling the user what they did wrong. 
		Card::Rank r = bidRank;
		for (int k = 0; k < 13; k++)
		{
			Card::Rank r = static_cast<Card::Rank>(k);
			if (r == bidRank)
			{
				rankBool = true;
			}
		}
		if (rankBool) 
		{
			rankBool = false;
			//Once a valid rank is input the hand will be looped through. Checking if the
			//player owns a card of their inputted rank.
			for (Card c : h.getHand()) 
			{
				if (c.getRank() == bidRank) 
				{
					rankBool = true;
					correctRank = true;
					//////////////////////////////////////////////////////////////////////////////
					//Added to fix a bug when a player was not cheating they were able to play a//
					//second card that was lower than the original bidded card. If the bid      //
					//before was a two, the players first none cheating bid could be three,     //
					//and their second card was able to be a two even when they chose to not    //
					//cheat                                                                     //
					//////////////////////////////////////////////////////////////////////////////
					if (!cheat && addBoolean)
					{
						if (rankChecker!= bidRank)
						{
							cout <<"Adding this card to your bid hand would be cheating" << endl;
							cout << "Enter a card of the rank " << rankChecker;
							addBoolean = true;
							return chooseBid(b, cheat);
						}
					}
					//////////////////////////////////////////////////////////////////////////////
					//breaks the for loop checking each card in hand if a correct card is found
					break;
				}
			}
		}
		if (rankBool) 
		{
			//If the player has chosen to not cheat, and they input a valid card that they
			//own this is now checking if the card they want to play would not be cheating
			if (!cheat) 
			{
				//If the input rank is equal to the previous bid rank this is a valid input
				if (bidRank == b.getRank()) 
				{
					correctRank = true;
					break;
					//If the input rank follows the previous bid rank this is a valid input
				} 
				else if (!bidRank == b.getNext()) 
				{
					correctRank = false;
					cout << "Playing a card of the rank "<< bidRank << " would be cheating" << endl;
					cout << "Enter a card to play:" << endl;
					cout << "First, the RANK of the card you would like to play" << endl;
				}
			}
		}
		else if (!correctRank) 
		{
			cout <<"You do not own the card of the rank " << bidRank << endl;
			cout << "Enter a card to play:" << endl;
			cout <<"First, the RANK of the card you would like to play" << endl;
		}
	}

	//Adds every card of given rank to a hand 
	Hand handOfRank = Hand();
	for (Card c : h.getHand()) {
		if (c.getRank() == bidRank) {
			handOfRank.add(c);
		}
	}
	bool suitBool = false;
	bool correctSuit = false;
	Card::Suit bidSuit;

	/*
	*Checking suit input is valid 
	*/
	cout<<"Now, enter the SUIT of the card that you would like to play" << endl;
	//Start of error checking for the SUIT input
	while (!correctSuit) 
	{
		cin.clear();
		cin >> bidSuit;
		//This will try to create a card using the suit the player has input.
		//If the player has input an invalid suit name the try will fail, and the 
		//while loop will restart, telling the user what they did wrong. 

		Card::Suit s = bidSuit;
		for (int k = 0; k < 4; k++)
		{
			Card::Suit s = static_cast<Card::Suit>(k);
			if (s == bidSuit)
			{
				suitBool = true;
			}
		}
		//Once a valid suit is input the hand containing every card of the previously
		//given rank will be looped through. Checking if the player owns a card of 
		//their inputted suit.
		if (suitBool) 
		{
			for (Card c : handOfRank.getHand()) 
			{
				if (c.getSuit() == bidSuit)
				{
					correctSuit = true;
					break;
				}
			}
		}
		if (!correctSuit) 
		{
			cout << "You do not own a " << bidRank << " of the Suit " << bidSuit << endl;
			cout << "Enter a card to play:" << endl;
			cout << "Enter the SUIT of the card you would like to play" << endl;
		}
	}
	/*
	* If the Player is cheating and is playing the first card they 
	* will be asked what rank they would like to lie about playing. 
	*/
	if (cheat && !addBoolean)
	{
		correctRank = false;
		rankBool = false;
		cout<< "What RANK would you like to lie about playing?" << endl;
		//Start of error checking for the false RANK input
		while (!correctRank)
		{
			cin.clear();
			cin >> cheatingRank;
			//Works in the same way as initial rank input

			Card::Rank r = cheatingRank;
			for (int k = 0; k < 13; k++)
			{
				Card::Rank r = static_cast<Card::Rank>(k);
				if (r == cheatingRank)
				{
					rankBool = true;
				}
			}

			//This checks that the play has input a rank that is not cheating as their
			// false rank
			if (rankBool) 
			{
				if (cheatingRank == b.getRank()) 
				{
					correctRank = true;
					break;
				}
				if (cheatingRank == b.getNext()) 
				{
					correctRank = true;
					break;
				}
			}
			if (!correctRank) 
			{
				cout << cheatingRank << " is not a valid rank" << endl;
				cout << b.getRank() << " was the last played rank. Your cheating bid rank must be either " << b.getRank() << " or " << b.getNext() << endl;
				cout << "What RANK would you like to lie about playing?" << endl;
			}

		}
	}
	//If the player is cheating and has entered a card correctly
	if (cheat)
	{
		//Turns the input into a Card object and adds this to the hand that
		//will be bid
		Card addCard = Card(bidRank, bidSuit);
		bidHand.add(addCard);
		//Removing this card from the players hand once bid
		h.remove(addCard);
		bool tempBool = false;

		//Asking if the player would like to add a new card when cheating
		while (!tempBool) 
		{
			cout << "Would you like to add another card? enter Y/N" << endl;
			string addAnother = "";
			cin.clear();
			cin >> addAnother;
			transform(addAnother.begin(), addAnother.end(), addAnother.begin(), ::tolower);	

			//Error checking, making sure that player input is valid
			if (addAnother == "y" || addAnother == "yes")
			{
				addBoolean = true;
				tempBool = true;
			}
			else if (addAnother == "n" || addAnother == "no")
			{
				addBoolean = false;
				tempBool = true;
			}
			else
			{
				cout << "Please enter a valid answer! \n" << endl;
				tempBool = false;
			}
		}
		//If playing is adding another card
		if (addBoolean) 
		{
			//Counting how many cards have been played                
			cheatersCardCount++;
			if (cheatersCardCount > 4) 
			{
				cout << "You cannot play more than 4 cards!" << endl;
				cout << "Enter any key to end your turn!" << endl;
				cin >> endTurn;
				cin.clear();
				Bid bid = Bid(bidHand, cheatingRank);
				bidHand = Hand();
				addBoolean = false;
				return bid;
			} 
			//If player is able to add another card
			else 
			{
				cout <<"You are now entering card number " << cheatersCardCount << " into your cheating bid" << endl;
				addBoolean = true;
				//Recursivly calls method to add another card to bidHand
				return chooseBid(b, cheat);

			}
		}
		//If no more cards are being added
		else 
		{
			cout << "Enter any key to end your turn!" << endl;
			cin >> endTurn;
			cin.clear();
			Bid bid = Bid(bidHand, cheatingRank);
			bidHand = Hand();
			addBoolean = false;
			return bid;
		}
	} 
	//If a player is not cheating they are asked to enter another card or not
	else 
	{
		//Turns the input into a Card object and adds this to the hand that will
		//be bid
		Card addCard = Card(bidRank, bidSuit);
		bidHand.add(addCard);
		//Removing this card from the players hand once bid
		h.remove(addCard);
		bool tempBool = false;

		//Asking if the player would like to add a new card when cheating
		while (!tempBool)
		{
			cout << "Would you like to add another card? enter Y/N" << endl;
			//Asking if the player would like to add a new card WHEN NOT CHEATING
			string addAnother = "";
			cin.clear();
			cin >> addAnother;
			//Error checking, making sure that player input is valid
			transform(addAnother.begin(), addAnother.end(), addAnother.begin(), ::tolower);	

			if (addAnother == "y" || addAnother == "yes")
			{
				addBoolean = true;
				tempBool = true;
			}
			else if (addAnother == "n"|| addAnother == "no")
			{
				addBoolean = true;
				tempBool = true;
			}
			else
			{
				cout << "Please enter a valid answer" << endl;
			}
		}
		//If the player has decided to add a new card
		if (addBoolean) 
		{
			bool haveCard = false;
			//Checks all cards in players hand and make sures they have another card 
			//playable without cheating
			for (Card c : h.getHand()) 
			{
				if (c.getRank() == addCard.getRank())
				{
					haveCard = true;
					break;
				}
			}
			//If player does not have a playable card without cheating
			if (!haveCard) 
			{
				cout << "Without cheating you have no more playable cards" << endl;
			} 
			else //If player does have a playable card
			{
				rankChecker = addCard.getRank();
				addBoolean = true;
				//Recursivly calls method to add another card to bidHand
				return chooseBid(b, cheat);
			}
			//Bid is created and returned if they get told they have more playable cards.
			Bid bid = Bid(bidHand, addCard.getRank());
			rankChecker = addCard.getRank();
			bidHand = Hand();
			addBoolean = false;
			cout << "Enter any key to end your turn!" << endl;
			cin >> endTurn;
			cin.clear();
			return bid;

			//If no new card is added this bid is returned.
		} 
		else 
		{
			Bid bid = Bid(bidHand, addCard.getRank());
			rankChecker = addCard.getRank();
			bidHand = Hand();
			addBoolean = false;
			cout << "Enter any key to end your turn!" << endl;
			cin >> endTurn;
			cin.clear();
			return bid;
		}
	}
}