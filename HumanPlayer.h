#pragma once
#include "Player.h"
class HumanPlayer : public Player
{
private:
	Hand h;

public:
	HumanPlayer(void);
	void addCard(Card c);
	void addHand(Hand hand);
	int cardsLeft();
	Bid playHand(Bid b);
	bool callCheat (Bid b);
	bool cheat(Bid b);
	Bid chooseBid (Bid b, bool cheat);

};

#include "HumanPlayer.hh"