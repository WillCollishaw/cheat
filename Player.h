#pragma once
#include "Hand.h"
#include "Bid.h"
class Player
{
private:
	Hand h;
public:
	Player();
	virtual void addCard(Card c) = 0;
	virtual void addHand(Hand h) = 0;
	virtual int cardsLeft() = 0;
	virtual Bid playHand(Bid b) = 0;
	virtual bool callCheat (Bid b) = 0;
	virtual bool cheat(Bid b) = 0;
	virtual Bid chooseBid (Bid b, bool cheat) = 0;

};

#include "Player.hh"